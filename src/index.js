const express = require('express')
const miniplug = require('miniplug')

const mp = miniplug({ guest: true })
const app = express()

// app.enable('trust proxy') // un-comment this line if the node app is behind a proxy, e.g nginx

app.set('json spaces', 4)
app.get('/:slug', async (req, res) => {
    try {
        if (!req.params.slug || req.params.slug === 'favicon.ico') return
        await mp.join(req.params.slug)
        console.log(`Getting room info for ${req.params.slug}`)
        const info = await mp.getRoomState()
        res.json(info)
    } catch (e) {
        console.error(`Error getting room info for ${req.params.slug}`)
        console.error(e)
        res.sendStatus(500)
    }
})

app.get('/:slug/history', async (req, res) => {
    try {
        if (!req.params.slug || req.params.slug === 'favicon.ico') return
        await mp.join(req.params.slug)
        console.log(`Getting room history for ${req.params.slug}`)
        const info = await mp.get('rooms/history')
        res.json(info)
    } catch (e) {
        console.error(`Error getting room history for ${req.params.slug}`)
        console.error(e)
        res.sendStatus(500)
    }
})

app.listen(3001, () => {
    console.log('app listening on port 3001!')
})
